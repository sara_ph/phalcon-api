<?php
// USE
use Phalcon\Mvc\Micro;
// REQUIRES
require(__DIR__ . "/App/config/loader.php");
$di = require(__DIR__ . "/App/config/di.php");
/// INIT
$app = new Micro();
date_default_timezone_set('Asia/Tehran');
// DI
$app->setDI($di);
// Before MiddleWare
$app->before(
    function () use($app, $di){
        $app->response->setHeader("Access-Control-Allow-Origin", "*")
            ->setHeader("Access-Control-Allow-Methods", 'GET,PUT,POST,DELETE,OPTIONS')
            ->setHeader("Access-Control-Allow-Headers", 'Origin, X-Requested-With, Content-Range, Content-Disposition, Content-Type, Authorization')
            ->setHeader("Access-Control-Allow-Credentials", true);
        if($_SERVER['REQUEST_METHOD'] != "OPTIONS" || $_SERVER['REQUEST_METHOD'] != "options")
            require (__DIR__ . "/App/middleware/acl.php");
    });
/// ROUTES
$app->options('/{catch:(.*)}', function() use ($app) {
    $app->response->setStatusCode(200, "OK")->send();
    return;
});
require __DIR__ . '/App/config/routes.php';
// After MiddleWare
$app->after(
    function () use ($app, $di) {
        $response = $app->getReturnedValue();
        if(isset($response['password'])) {
            unset($response['password']);
        }
        if (is_array($response)) {
            $app->response->setJsonContent($response);
        }
        if(!$app->response->isSent()) {
            $app->response->send();
        }
    // $app->stop();
    });
$app->notFound(function ()use ($app){
   $app->getDI()->get("sendError", [$app, 404, "Not Found", "Not Found Page"]);
   $app->stop();
});
try {
    $app->handle();
} catch (Exception $ex) {
    var_dump($ex->getMessage());
}
