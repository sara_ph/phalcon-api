<?php
namespace App\Models;

use Phalcon\Mvc\Model;

class Services extends Model{
    public $id;
    public $name;
    public $description;
    public $price;
    public $assessments_id;
    public function initialize()
    {
        $this->hasMany(
            'id',
            'App\Models\UserServices',
            'service_id',
            ['alias'=>'UserServices']
        );
        $this->hasOne(
          "assessment_id",
          "App\Models\Assessments",
          "id",
            ['alias'=>'Assessment', "reusable"=>true]

        );
    }
}
