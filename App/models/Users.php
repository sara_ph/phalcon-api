<?php

namespace App\Models;
use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness;

class Users extends Model
{

    public $id;
    public $username;
    public $password;
    public $firstName;
    public $lastName;
    public $role;
    public $token;
    public function initialize()
    {
        $this->hasMany(
            'id',
            'App\Models\UserServices',
            'user_id',
            ['alias'=>'UserServices']
        );
    }
    public function validation()
    {
        $validator = new Validation();
        $validator->add(
            'username',
            new Uniqueness(
                [
                    'field' => 'username',
                    'message' => 'The username must be unique',
                ]
            )
        );

        return $this->validate($validator);
    }

}
