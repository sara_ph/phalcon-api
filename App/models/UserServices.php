<?php
namespace App\Models;

use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness;

class UserServices extends Model{
    public $id,
        $user_id,
        $service_id,
        $finished,
        $startDate,
        $finishDate,
        $result;
    public function initialize()
    {
        $this->belongsTo(
            'user_id',
            'App\Models\Users',
            'id',
            [
                'alias' => 'Users',
                'foreignKey' => [
                                    'message' => 'The user_id does not exist on the Users model.'
                                ]
            ]
        );

        $this->belongsTo(
            'service_id',
            'App\Models\Services',
            'id',
            [
                'alias'=>'Service',
                'foreignKey' => [
                    'message' => 'The service_id does not exist on the Services model.'
                ]
            ]
        );
    }
    public function validation()
    {
        $validator = new Validation();
        $validator->add(['user_id', 'service_id'],
            new Uniqueness(
                [
                    "field"=>['user_id', 'service_id'],
                    'message' => 'The user has already bought this service.',
                ]
            )
        );

        return $this->validate($validator);
    }

}
