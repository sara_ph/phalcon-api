<?php
namespace App\Models;

use Phalcon\Mvc\Model;

class Assessments extends Model{
    public $id;
    public $structure;
    public function initialize()
    {
        $this->belongsTo(
            'id',
            'App\Models\Services',
            'assessment_id',
            ['alias'=>'Service', "reusable"=>true]
        );
    }
}
