<?php

namespace App\Api;

use Phalcon\DI\Injectable;

class MainController extends Injectable
{
    public function sendError ($errorCode, $errorMessageHeader, $errorMessageBody = null)
    {
        $this->response->setStatusCode($errorCode, $errorMessageHeader);
        if (!isset($errorMessageBody)) {
            $errorMessageBody = $errorMessageHeader;
        }
        $this->response->setJsonContent(["errorCode" => $errorCode, "errorMessage" => $errorMessageBody]);
        $this->response->send();
    }
}