<?php
namespace App\Api\Users;
use Phalcon\Mvc\Micro\Collection;
$collection = new Collection();
$collection->setHandler('App\Api\Users\Controller', true);
$collection->setPrefix('/users');
/// Create
$collection->post('/', 'createUserAction');
/// Login
$collection->post('/login', 'loginAction');
/// Read
$collection->get('/', 'getUsersAction');
$collection->get('/{userId:[1-9][0-9]*}', 'getUserByIdAction');
/// Update
$collection->put('/{userId:[1-9][0-9]*}', 'updateUserAction');
/// Delete
$collection->delete('/{userId:[1-9][0-9]*}', 'deleteUserAction');
