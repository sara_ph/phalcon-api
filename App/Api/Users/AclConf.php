<?php
return [
    "accessRoles"=>[
        'guest' => [
            'createUserAction',
            'loginAction',
                        'getUsersAction',
            'getUserByIdAction',
            'updateUserAction',
        ],
        "user" => [

        ],
        "admin" => [
            'deleteUserAction'
        ]
    ],
    "accessList"=>[
        'createUserAction',
        'loginAction',
        'getUsersAction',
        'getUserByIdAction',
        'updateUserAction',
        'deleteUserAction'
    ]
];
