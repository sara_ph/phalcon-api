<?php

namespace App\Api\Users;
use App\Api\MainController;
use App\Models\Users;
use Phalcon\Crypt;

class Controller extends MainController
{
    public function createUserAction()
    {
        $Users = new Users();
        $reqBody = $this->request->getJsonRawBody(true);
        $reqBody["password"] = $this->security->hash($reqBody["password"]);
        unset($reqBody["role"]);
        if (!$Users->create($reqBody)) {
            $this->sendError(422, "Unprocessable Entity", $Users->getMessages()[0]->getMessage());
        }
        /**
         * @var $user Users
         */
        $user = Users::findFirst([
            'conditions' => 'username = :username:',
            'bind' => [
                "username" => $reqBody['username']
            ]
        ]);
        $user->token = $this->generateToken($user->id);
        $user->save();
        $this->response->setStatusCode(201, 'Created');
        return $user->toArray();
    }

    public function loginAction()
    {
        $reqBody = $this->request->getJsonRawBody(true);
        /**
         * @var $user Users
         */
        $user = Users::findFirst([
            'conditions' => 'username = :username:',
            'bind' => [
                "username" => $reqBody['username']
            ]
        ]);
        if ($user) {
            $check = $this
                ->security
                ->checkHash($reqBody["password"], $user->password);
            if ($check) {
                $user->token = $this->generateToken($user->id);
                $user->save();
                return [
                    'id' => $user->id,
                    'username' => $user->username,
                    'token' => $user->token
                ];
            }
        }
        return $this->sendError(404, "Not Found", "The password or username are incorrect!");
    }

    public function getUsersAction()
    {
        $users = Users::find(["columns" => "id, username, firstName, lastName, role", 'order' => 'id']);
        return ($users->toArray());
    }

    public function getUserByIdAction($userId)
    {
        $user = Users::findFirst([
            'conditions' => 'id = :id:',
            'bind' => [
                "id" => $userId
            ],
            "columns" => "id, username, firstName, lastName",

        ]);
        if (!$user) {
            return $this->sendError(404, "Not Found!");
        }
        return ($user->toArray());
    }

    public function updateUserAction($userId)
    {
        if ($this->getDI()->get("tokenData")["role"] != "admin") {
            if ($userId !== $this->getDI()->get("tokenData")["id"]) {
                return $this->sendError(403, "Permission denied!");
            }
        }
        $reqBody = $this->request->getJsonRawBody(true);
        unset($reqBody["role"]);
        $reqBody["password"] = $this->security->hash($reqBody["password"]);
        $user = Users::findFirst([
            'conditions' => 'id = :id:',
            'bind' => [
                "id" => $userId
            ]
        ]);
        if (!$user) {
            return $this->sendError(404, "Not Found!");
        }
        if (!$user->update($reqBody)) {
            return $this->sendError(422, "Unprocessable Entity", $user->getMessages()[0]->getMessage());
        }
        return ($user->toArray());
    }

    public function deleteUserAction($userId)
    {
        if ($this->getDI()->get("tokenData")["role"] != "admin") {
            if ($userId !== $this->getDI()->get("tokenData")["id"]) {
                return $this->sendError(403, "Permission denied!");
            }
        }
        $user = Users::findFirst([
            'conditions' => 'id = :id:',
            'bind' => [
                "id" => $userId
            ]
        ]);
        if (!$user) {
            return $this->sendError(404, "Not Found!");
        }
        $user->delete();
        return ($user->toArray());
    }

    private function generateToken($userId)
    {
        $crypt = new Crypt('aes-256-cfb', true);
        $data = [
            'id' => $userId,
            'exp' => 3360
        ];
        $dataEn = json_encode($data);
        return $crypt->encryptBase64($dataEn, $this->getDI()->getDefault()->get("keyAuth"));
    }

    public static function getControllerName()
    {
        return "users";
    }
}
