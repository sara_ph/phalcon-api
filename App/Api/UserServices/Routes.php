<?php
namespace App\Api\UserServices;
use Phalcon\Mvc\Micro\Collection;

$collection = new Collection();
$collection->setHandler('App\Api\UserServices\Controller', true);
$collection->setPrefix('/user-services');
/// Create
$collection->post('/', 'createUserServiceAction');
/// Read
$collection->get('/', 'getUserServicesAction');
$collection->get('/getUserServiceById/{userId:[1-9][0-9]*}','getUserServiceByIdAction');
$collection->get('/getServicesByUserId/{userId:[1-9][0-9]*}','getServicesByUserIdAction');
///// Update
$collection->put('/{id:[1-9][0-9]*}', 'updateUserServiceAction');
///// Delete
$collection->delete('/{id:[1-9][0-9]*}', 'deleteUserServiceAction');
///// Read Service Structure
$collection->get('/structure/{id:[1-9][0-9]*}', 'getUserServiceStructureAction');
///// Respond
$collection->post('/structure/{id:[1-9][0-9]*}', 'creatUserServiceAnswerAction');




