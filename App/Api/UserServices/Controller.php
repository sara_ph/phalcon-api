<?php

namespace App\Api\UserServices;

use App\Api\MainController;
use App\Models\UserServices;
use PDOException;
use Phalcon\Db;

class Controller extends MainController
{
    public function createUserServiceAction()
    {
        $UserServices = new UserServices();
        $reqBody = $this->request->getJsonRawBody(true);
        $reqBody["startDate"] = date("Y-m-d");
        $reqBody["finishDate"] = null;
        $reqBody["finished"] = false;
        try {
            if (!$UserServices->create($reqBody)) {
                return $this->sendError(422, "Unprocessable Entity", $UserServices->getMessages()[0]->getMessage());
            }
            $this->response->setStatusCode(201, 'Created');
            return $reqBody;
        } catch (PDOException $e) {
            return $this->sendError(422, "Unprocessable Entity", $e->getMessage());
        }
    }

    public function getUserServicesAction()
    {
        $q = "SELECT u_s.id, u_s.user_id, u_s.service_id, u_s.finished, u_s.startDate, u_s.finishDate,
                    u.username, u.firstName, u.lastName, u.role
                FROM user_services u_s INNER JOIN users u ON u_s.user_id = u.id ORDER BY u_s.id";
        return $this->db->fetchAll($q, Db::FETCH_ASSOC);
    }

    public function getUserServiceByIdAction($userServiceID)
    {
        $q = "SELECT u_s.id, u_s.user_id, u_s.service_id, u_s.finished, u_s.startDate, u_s.finishDate, u_s.result,
                    u.username, u.firstName, u.lastName, u.role
                FROM user_services u_s INNER JOIN users u ON u_s.user_id = u.id WHERE u_s.id IN (:userServiceId) ORDER BY u_s.id";
        return $this->db->fetchAll($q, Db::FETCH_ASSOC, [
            "userServiceId" => $userServiceID
        ]);
    }

    public function getServicesByUserIdAction($userID)
    {
        $q = "SELECT u_s.id, u_s.user_id, u_s.service_id, u_s.finished, u_s.startDate, u_s.finishDate,
                    u.username, u.firstName, u.lastName, u.role
                FROM user_services u_s INNER JOIN users u ON u_s.user_id = u.id WHERE u.id IN (:userID) ORDER BY u_s.id";
        return $this->db->fetchAll($q, Db::FETCH_ASSOC, [
            "userID" => $userID
        ]);
    }

    public function updateUserServiceAction($id)
    {
        /**
         * @var $userService UserServices
         */
        $userService = UserServices::findFirst([
            'conditions' => 'id = :id:',
            'bind' => [
                "id" => $id
            ]
        ]);
        if ($this->getDI()->get("tokenData")["role"] != "admin") {
            if ($userService->user_id !== $this->getDI()->get("tokenData")["id"]) {
                return $this->sendError(403, "Forbidden", "Permission denied!");
            }
        }
        if (!$userService) {
            return $this->sendError(404, "Not Found!");
        }
        if ($userService->finished) {
            return $this->sendError(422, "Unprocessable", "This service was already finished!");
        }
        $userService->finishDate = date("Y-m-d");
        $userService->finished = 1;
        if (!$userService->save()) {
            var_dump($userService->getMessages());
        }
        return ($userService->toArray());
    }

    public function deleteUserServiceAction($id)
    {
        /**
         * @var $userService UserServices
         */
        $userService = UserServices::findFirst([
            'conditions' => 'id = :id:',
            'bind' => [
                "id" => $id
            ]
        ]);
        if ($this->getDI()->get("tokenData")["role"] != "admin") {
            if ($userService->user_id !== $this->getDI()->get("tokenData")["id"]) {
                return $this->sendError(403, "Forbidden", "Permission denied!");
            }
        }
        if (!$userService) {
            return $this->sendError(404, "Not Found!");
        }
        $userService->delete();
        return $userService->toArray();
    }

    public function getUserServiceStructureAction($id)
    {
        $userService = UserServices::findFirst([
            'conditions' => 'id = :id:',
            'bind' => [
                "id" => $id
            ]
        ]);
        $data = (json_decode($userService->Service->getAssessment()->structure, true));
//        $data= (array) $data;
        array_walk($data, function (&$value) {
            $value = (array)$value;
            unset($value["answers"][0]->index);
            unset($value["answers"][1]->index);
        });
        return $data;
    }

    public function creatUserServiceAnswerAction($id)
    {
        /**
         * @var $userService UserServices
         */
        $userService = UserServices::findFirst([
            'conditions' => 'id = :id:',
            'bind' => [
                "id" => $id
            ]
        ]);
        $reqBody = $this->request->getJsonRawBody(true);
        $mbtiData = json_decode($userService->getService()->getAssessment()->toArray()["structure"], true);
        $index = ["i" => 0, "e" => 0, "s" => 0, "t" => 0, "n" => 0, "f" => 0, "j" => 0, "p" => 0];
        for ($i = 1; $i < 61; $i++) {
            if (!isset($reqBody[$i])) {
                $this->sendError(422, "The " . $i . "th answer not set");
            }
            if ($reqBody[$i] == 1) {
                $indexTemp = $mbtiData[$i]["answers"][0]["index"];
            } elseif ($reqBody[$i] == 2) {
                $indexTemp = $mbtiData[$i]["answers"][1]["index"];
            } else {
                return $this->sendError(422, "Unprocessable", "The " . $i . "th answer is unprocessable, please select 1 or 2");
            }
            $index[$indexTemp]++;
        }
        $userService->finishDate = date("Y-m-d");
        $userService->finished = 1;
        $userService->result = json_encode($index, JSON_UNESCAPED_UNICODE);
        $userService->save();

        return $index;
    }

    public function getControllerName(): string
    {
        return "userServices";
    }
}
