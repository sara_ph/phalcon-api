<?php
return [
    "accessRoles"=>[
        'guest' > [

        ],
        "user" => [
            'createUserServiceAction',
            'getUserServicesAction',
            'updateUserServiceAction',
            'getUserServiceStructureAction',
            'creatUserServiceAnswerAction',
            'getServicesByUserIdAction',
            'getUserServiceByIdAction'
        ],
        "admin" => [
            'deleteUserServiceAction'
        ]
    ],
    "accessList"=>[
        'createUserServiceAction',
        'getUserServicesAction',
        'getUserServiceByIdAction',
        'getServicesByUserIdAction',
        'updateUserServiceAction',
        'deleteUserServiceAction',
        'getUserServiceStructureAction',
        'creatUserServiceAnswerAction'
    ]
];