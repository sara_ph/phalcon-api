<?php

namespace App\Api\Services;

use App\Models\Services;
use PDOException;
use App\Api\MainController;

class Controller extends MainController
{
    public function createServiceAction()
    {

        $Services = new Services();
        $reqBody = $this->request->getJsonRawBody(true);
        if( !$Services->create($reqBody)){
            return $this->sendError(422, "Unprocessable Entity",
                $Services->getMessages()[0]->getMessage());
        }
        $this->response->setStatusCode(201, "Created");
        return $reqBody;
    }

    public function getServicesAction()
    {
        $services = Services::find();
        return $services->toArray();
    }

    public function getServiceByIdAction($serviceId)
    {
        $service = Services::findFirst([
            'conditions' => 'id = :id:',
            'bind' => [
                "id" => $serviceId
            ],
            "columns" => "id, name, description, price",

        ]);
        if (!$service) {
            return $this->sendError($this, 404, "Not Found!");
        }
        return ($service->toArray());
    }

    public function updateServicesAction($serviceId)
    {
        $reqBody = $this->request->getJsonRawBody(true);
        $service = Services::findFirst([
            'conditions' => 'id = :id:',
            'bind' => [
                "id" => $serviceId
            ]
        ]);
        if (!$service) {
            $this->sendError(404, "Not Found");
        }
        $service->update($reqBody);
        return $service->toArray();
    }

    public function deleteServiceAction($serviceId)
    {
        $service = Services::findFirst([
            'conditions' => 'id = :id:',
            'bind' => [
                "id" => $serviceId
            ]
        ]);
        if (!$service) {
            $this->sendError(404, "Not Found");
        }
        try{
            if(!$service->delete()){
                return $this->sendError(422, "Unprocessable Entity",
                    $service->getMessages()[0]->getMessage());
            }
            $data = $service->toArray();
            return ($data);
        } catch(PDOException $e){
            return $this->sendError(503, "Unprocessable Entity", $e->getMessage());
        }
    }
    public static function getControllerName()
    {
        return "services";
    }
}
