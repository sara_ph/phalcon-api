<?php
return [
    "accessRoles"=>[
        'guest' > [
        ],
        "user" => [
            'getServicesAction',
            'getServiceByIdAction'
        ],
        "admin" => [
            'createServiceAction',
            'updateServicesAction',
            'deleteServiceAction'
        ]
    ],
    "accessList"=>[
        'getServicesAction',
        'createServiceAction',
        'updateServicesAction',
        'deleteServiceAction',
        'getServiceByIdAction'
    ]
];