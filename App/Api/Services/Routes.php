<?php
namespace App\Api\Services;
use Phalcon\Mvc\Micro\Collection;
$collection = new Collection();
$collection->setHandler('App\Api\Services\Controller', true);
$collection->setPrefix('/services');
/// Create
$collection->post('/', 'createServiceAction');
/// Read
$collection->get('/', 'getServicesAction');
$collection->get('/{serviceId:[1-9][0-9]*}','getServiceByIdAction');
/// Update
$collection->put('/{serviceId:[1-9][0-9]*}', 'updateServicesAction');
/// Delete
$collection->delete('/{serviceId:[1-9][0-9]*}', 'deleteServiceAction');
