<?php

use Phalcon\Config;

return new Config(
    [
        'database' => [
            'adapter' => 'mysql',
            'host' => 'localhost',
            'port' => 3306,
            'username' => 'root',
            'password' => '1377',
            'dbname' => 'milogy_api',
        ],

        'application' => [
            // 'controllersDir' => "App/Api/",
            'modelsDir' => "App/models/",
            'baseUri' => "/",
        ],
    ]);
