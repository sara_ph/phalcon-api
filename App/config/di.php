<?php

use App\Middleware\MyDbListener;
use Phalcon\Db\Adapter\Pdo\Mysql;
use Phalcon\Db\Profiler;
use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\Model\Metadata\Memcache;
use Phalcon\Http\Response;
use Phalcon\Events\Manager as EventsManager;


// Initializing a DI Container
$di = new FactoryDefault();
$config = require(__DIR__ . "/config.php");
$dbListener = new MyDbListener();

/** Common config */
$di->setShared('config', $config);
/** Error Handling */
$di->setShared("sendError", function ($app, $errorCode, $errorMessageHeader, $errorMessageBody = null) {
    $app->response->setStatusCode($errorCode, $errorMessageHeader);
    if (!isset($errorMessageBody)) {
        $errorMessageBody = $errorMessageHeader;
    }
    $app->response->setJsonContent(["errorCode" => $errorCode, "errorMessage" => $errorMessageBody]);
    $app->response->send();
});
/** Profiler */
$di->setShared('profiler', function () {
    return new Profiler();
});

/** Database */
$di->set(
    "db",
    function () use ($config, $dbListener) {
        $connection = new Mysql(
            [
                "host" => $config->database->host,
                "username" => $config->database->username,
                "password" => $config->database->password,
                "dbname" => $config->database->dbname,
            ]
        );

        $eventsManager = new EventsManager();
        $eventsManager->attach(
            'db',
            $dbListener
        );

        $connection->setEventsManager($eventsManager);
        return $connection;
    }
);
/** Cache */
$di->set('modelsMetadata', function(){
    return new Memcache([
        'lifetime' => 86400,
        'port' => 11211
    ]);
});
/** Response */
$di->setShared(
    'response',
    function (){
        $response = new Response();
        $response->setContentType('application/json', 'utf-8');
        $response->sendHeaders();
        return $response;
    });
$di->setShared("keyAuth", function () {
    return "T4\xb1\x8d\xa9\x98\x05\\x8c\xbe\x1d\x07&[\x99\x18\xa4~Lc1\xbeW\xb3";
});

return $di;
