<?php

use Phalcon\Loader;

$loader = new Loader();
$loader->registerNamespaces(
    [
        'App\Api' => realpath(__DIR__ . '/../Api/'),
        'App\Models' => realpath(__DIR__ . '/../models/'),
        'App\Middleware' => realpath(__DIR__.'/../middleware')
    ]
);

$loader->register();