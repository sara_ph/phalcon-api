<?php
$select = $app->request->getURI();
$pos2 = strpos($select, "/", strpos($select, "/", 0)+1);
if($pos2)
    $select = substr($select, 0, $pos2);
switch ($select) {
    case '/users':
        require "App/Api/Users/Routes.php";
        break;
    case '/services':
        require "App/Api/Services/Routes.php";
        break;
    case '/user-services':
        require "App/Api/UserServices/Routes.php";
        break;
    default:
        echo "404 Page Not Found";
        break;
}

/* Mount */
if (isset($collection))
    $app->mount($collection);

