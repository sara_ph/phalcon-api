<?php
namespace App\Middleware;

use Phalcon\Events\Event;
use Phalcon\Db\Profiler;
use Phalcon\Logger;
use Phalcon\Logger\Adapter\File as FileAdapter;

class MyDbListener
{
    protected $profiler;
    protected $logger;

    /**
     * Creates the profiler and starts the logging
     */
    public function __construct()
    {
        $this->profiler = new Profiler();
        $this->logger   = new FileAdapter(__DIR__.'/../logs/db.log');
    }

    /**
     * This is executed if the event triggered is 'beforeQuery'
     * @param $connection
     */
    public function beforeQuery(Event $event, $connection)
    {
        $this->profiler->startProfile(
            $connection->getSQLStatement()
        );
    }
    /**
     * This is executed if the event triggered is 'afterQuery'
     * @param $connection
     */
    public function afterQuery(Event $event, $connection)
    {
        $this->logger->log(
            $connection->getSQLStatement(),
        );

        $this->profiler->stopProfile();
    }

    public function getProfiler()
    {
        return $this->profiler;
    }
}


