<?php

use Phalcon\Crypt;
use Phalcon\Di;

function parseToken($token)
{
    $crypt = new Crypt('aes-256-cfb', true);
    try {
        $dataJson = $crypt->decryptBase64($token, DI::getDefault()->get("keyAuth"));
    } catch (Crypt\Mismatch $e) {
        return false;
    }
    if (!$dataJson) {
        return false;
    }
    return json_decode($dataJson, true);
}