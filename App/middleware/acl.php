<?php

use App\Models\Users;
use Phalcon\Acl;
use Phalcon\Acl\Adapter\Memory as AclList;

//// Targets
$targetResource = $app->getActiveHandler()[0]->getDefinition()::getControllerName();
$targetAccess = $app->getActiveHandler()[1];
// Generate ACL
$acl = new AclList();
// Default action is deny access
$acl->setDefaultAction(
    Acl::DENY
);
// Create some roles.
$roleAdmin = new Acl\Role('admin', 'Super-User role');
$roleUsers = new Acl\Role('user', 'Registered user');
$roleGuests = new Acl\Role('guest', 'Not registered user');
// Add role to ACL
$acl->addRole($roleGuests);
$acl->addRole($roleUsers);
$acl->addRole($roleAdmin);
$acl->addInherit($roleUsers,$roleGuests);
$acl->addInherit($roleAdmin,$roleUsers);
// Find Target Resource
switch ($targetResource){
    case 'users':
        $resource = new Acl\Resource('users');
        $aclConf=require "App/Api/Users/AclConf.php";
        break;
    case "services":
        $resource = new Acl\Resource('services');
        $aclConf=require "App/Api/Services/AclConf.php";
        break;
    case "userServices":
        $resource = new Acl\Resource('userServices');
        $aclConf=require "App/Api/UserServices/AclConf.php";
        break;
    default:
        $di->getSendError($app, 404, "Not Found");
        $app->stop();
        break;
}

$accessList=$aclConf["accessList"];
// Add Resource
$acl->addResource($resource,$accessList);
//
$accessRoles=$aclConf["accessRoles"];
// Set Access Allow
foreach($accessRoles as $roleKey=>$list){
    if (is_array($list)) {
        foreach ($list as $accessValue) {
            if ($accessValue)
                $acl->allow($roleKey, $resource->getName(), $accessValue);
        }
    }
}
if($acl->isAllowed("guest", $targetResource, $targetAccess)) {
    return true;
}
////////
// Ath
require __DIR__ . "/auth.php";
$token = $app->request->getHeader("Authorization");
if (empty($token)) {
    $targetRole = "guest";
} else {
    $token = substr($token, 7, strlen($token));
    if (empty($token)){
        $di->getSendError($app, 401, "Unauthorized","Authentication failed!");
        return false;
    }else {
        $tokenData = parseToken($token);
    }
    if (!$tokenData) {
        $di->getSendError($app, 401, "Unauthorized","Authentication failed!");
        $app->stop();
    } else {
        /**
         * @var $user users
         */
        $user = Users::findFirst([
            "columns" => "role, token",
            'conditions' => 'id = ?1',
            'bind' => [
                1 => $tokenData["id"]
            ]
        ]);
        if($user->token !== $token) {
            $di->getSendError($app, 401, "Unauthorized","Authentication failed!");
            $app->stop();
        }
        $targetRole = $user->role;
    }
}

$tokenData["role"]=$targetRole;
$di->set("tokenData", function () use ($tokenData){
    return $tokenData;
});
if($acl->isAllowed($targetRole, $targetResource, $targetAccess))
    return true;
else{
    $di->getSendError($app, 403, "Permission denied!");
    $app->stop();
}
